#!/bin/bash
set -x
if [ -d FlameGraph ]; then
   rm -rf FlameGraph
fi
git clone https://github.com/brendangregg/FlameGraph
set +x
