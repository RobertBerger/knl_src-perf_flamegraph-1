HERE=$(pwd)
set -x
cd FlameGraph
perf script | ./stackcollapse-perf.pl > ${HERE}/out.perf-folded
cd ${HERE}
set +x
