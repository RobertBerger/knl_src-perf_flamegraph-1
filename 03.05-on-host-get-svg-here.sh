OLD_PATH=$PATH
PWD=`pwd`

source ../../env.sh

set -x
sudo cp $NFS_EXPORTED_ROOTFS_ROOT/`pwd`/perf-kernel.svg .
sudo chown ${USER}:${USER} perf-kernel.svg
set +x

PATH=$OLD_PATH
cd $PWD
