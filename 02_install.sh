HERE=$(pwd)

source ../../env.sh

if [ -d ${NFS_EXPORTED_ROOTFS_ROOT}/$(pwd) ]; then
   sudo rm -rf ${NFS_EXPORTED_ROOTFS_ROOT}/$(pwd)
fi

sudo mkdir -p ${NFS_EXPORTED_ROOTFS_ROOT}/$(pwd)
sudo chown ${USER}:${USER} ${NFS_EXPORTED_ROOTFS_ROOT}/$(pwd)
cp -r * ${NFS_EXPORTED_ROOTFS_ROOT}/$(pwd)

cd ${HERE}
