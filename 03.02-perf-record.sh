HERE=$(pwd)
set -x
stress-ng --cpu 4 --io 4 --vm 2 --vm-bytes 128M --fork 4 --timeout 60s &
cd FlameGraph
perf record -F 99 -ag -- sleep 60
cd ${HERE}
set +x
