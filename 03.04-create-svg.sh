HERE=$(pwd)
set -x
cd FlameGraph
cat ${HERE}/out.perf-folded | ./flamegraph.pl > ${HERE}/perf-kernel.svg
cd ${HERE}
set +x
